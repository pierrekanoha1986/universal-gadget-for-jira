#Why Universal Gadget for JIRA#
After developing few custom JIRA gadgets that relied on data retrieved by JavaScript from JIRA REST API and external services, I asked myself: "why should I develop and package new gadget every time". Since JavaScript is dynamically interpreted, we can have some basic JIRA gadget plugin and give ability to add custom JavaScript code. The custom code will be loaded into the gadget iframe and executed there.

So whenever I need to show a table or a chart with some data that are retrieved through JIRA REST API, I just write a JavaScript that fetches and displays the data. No need to create and package new gadget every time.
#How it works#
Configured script and css files are attached to the iframe. The gadget configuration screen has following fields:

**Gadget title** - put the title you want for your gadget instance.

**JavaScript URLs** - comma separated list of URLs to JavaScript files. These can be your custom JavaScript or links to hosted libraries like https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js.

**CSS URLs** - comma separated list of URLs to CSS files. Again, can be your custom styles or externally hosted.

**HTML to append** - any valid HTML combined with inline JavaScript or inline CSS to be added as the top element of the gadget view. Usually to add parent container and initialization JavaScript.

**Predefined scripts** - Select with samples to demonstrate what can be built based on Universal Gadget for JIRA. "JavaScript URLs", "CSS URLs" and "HTML to append" are populated when you select some predefined sample. Currently there are 2 samples: Weather sample script and User timesheet sample script

**Refresh Interval** - This is a standard option to configure refreshment of the gadget. For the consequent refreshes only the content from "HTML to append" field is evaluated. To optimize refreshment the JavaScitpt(s) and CSS(es) configured in "JavaScript URLs" and "CSS URLs" are fetched and evaluated only first time when the gadged is loaded. So please make sure to put the JavaScript code responsible for the refreshes to the "HTML to append" field. Usually this should be code that invokes some method declared in files from "JavaScript URLs" field.

The plugin defines 2 global varaibles that you can access from your custom JavaScript.

**__gadget** is a gadget JavaScript object. Please see here https://developer.atlassian.com/display/GADGETS/Creating+a+Gadget+JavaScript+Object for more details.

**__gadgetArgs** is an object that for now holds only user object:

```
#!javascript

{
  "username": "admin",
  "fullName": "Volodymyr Krupach [Administrator]",
  "email": "admin@admin.net"
}
```
#License and sources#
Universal Gadget for JIRA is released under the FreeBSD license, which puts almost no restrictions on how you can use it. Please fill free to use Universal Gadget for JIRA in your commercial projects or as part of services you provide.

Universal Gadget for JIRA bitbucket: https://bitbucket.org/vkrupach/universal-jira-gadget

Predefined samples bitbucket: https://bitbucket.org/vkrupach/universal-jira-gadget-samples
#Todo#
It would be nice to give ability to configure widget visibility on the widget configuration screen. I know it can be configured through "roles-required” param inside of the gadget tag, but this requires a string and does not give possibility to pass value from configurations. I found description of enabled-conditions and local-conditions conditions: https://developer.atlassian.com/display/GADGETS/Packaging+your+Gadget+as+an+Atlassian+Plugin but looks like they are not parsed by JIRA at all. At least they do not work for me and I see nothing when search globally through JIRA sources.

Please email me support at jira-work-calendar.com any bugs and ideas for improvement. Also you can create a bitbucket pull request.