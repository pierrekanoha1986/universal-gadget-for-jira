package com.ujg;

import java.util.Locale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.ujg.controller.UniversalGadgetServlet;


@SpringBootApplication
public class AddonApplication {

  public static void main(String[] args) throws Exception {
    new SpringApplication(AddonApplication.class).run(args);
  }

  @Bean
  public LocaleResolver localeResolver() {
    SessionLocaleResolver slr = new SessionLocaleResolver();
    slr.setDefaultLocale(Locale.US); // Set default Locale as US
    return slr;
  }

  @Bean
  public ResourceBundleMessageSource messageSource() {
    ResourceBundleMessageSource source = new ResourceBundleMessageSource();
    source.setBasenames("i18n/ujg"); // name of the resource bundle
    source.setAlwaysUseMessageFormat(true); // always write ''
    source.setUseCodeAsDefaultMessage(true);
    return source;
  }

  @Bean
  public ServletRegistrationBean registerTimeGadgetServlet(DispatcherServlet dispatcherServlet) {
    return new ServletRegistrationBean(new UniversalGadgetServlet(), "/plugins/servlet/ujg");
  }

}