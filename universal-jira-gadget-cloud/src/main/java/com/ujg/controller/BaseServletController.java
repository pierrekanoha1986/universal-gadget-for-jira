package com.ujg.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public abstract class BaseServletController extends HttpServlet {

  /**
   * Generated UID.
   */
  private static final long serialVersionUID = -3153802382858250580L;

  /**
   * AutowireCapableBeanFactory.
   */
  protected AutowireCapableBeanFactory ctx;

  @Override
  public void init() throws ServletException {
    super.init();
    WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
    // wire the servlet to spring
    ctx = context.getAutowireCapableBeanFactory();
    ctx.autowireBean(this);
  }

}