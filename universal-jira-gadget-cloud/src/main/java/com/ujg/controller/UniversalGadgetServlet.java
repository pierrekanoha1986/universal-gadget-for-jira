package com.ujg.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import com.ujg.common.service.VelocityRenderer;


@SuppressWarnings("serial")
public class UniversalGadgetServlet extends BaseServletController {

  @Autowired
  private VelocityRenderer velocityRenderer;

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    Map<String, Object> contextParamsMap = new HashMap<>();
    velocityRenderer.renderTemplate(req, resp, "/templates/ujg/cloudPage.vm", contextParamsMap);
  }

}