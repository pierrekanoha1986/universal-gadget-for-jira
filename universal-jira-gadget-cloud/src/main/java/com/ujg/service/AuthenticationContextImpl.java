package com.ujg.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.auth.jwt.JwtAuthentication;
import com.atlassian.connect.spring.internal.jwt.Jwt;
import com.google.gson.internal.LinkedTreeMap;
import com.ujg.common.model.User;
import com.ujg.common.service.AuthenticationContext;
import com.ujg.common.utils.CommonUtils;


@Component
public class AuthenticationContextImpl implements AuthenticationContext {

  @Override
  public User getUser() {
    User user = null;
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (auth instanceof JwtAuthentication) {
      JwtAuthentication jwtAuth = (JwtAuthentication) auth;
      AtlassianHostUser ahUser = (AtlassianHostUser) jwtAuth.getPrincipal();
      if (ahUser.getUserKey().isPresent()) {
        Map<String, String> userMap = parseUserMap(jwtAuth);
        String userKey = ahUser.getUserKey().get();
        String username = userMap.containsKey("username") ? userMap.get("username") : userKey;
        String displayName = userMap.containsKey("displayName") ? userMap.get("displayName") : username;
        user = new User(userKey, username, displayName);
      }
    }
    return user;
  }

  /**
   * Try to get username and displayName from the json passed from the host JIRA
   * 
   * @param jwtAuth
   * @return
   */
  @SuppressWarnings("unchecked")
  private Map<String, String> parseUserMap(JwtAuthentication jwtAuth) {
    Map<String, String> userMap = new HashMap<>(); // mepty map by default
    Jwt jwt = (Jwt) jwtAuth.getCredentials();
    @SuppressWarnings("rawtypes")
    LinkedTreeMap jwtMap = CommonUtils.jsonStringToObject(jwt.getJsonPayload(), LinkedTreeMap.class);
    if (jwtMap != null && jwtMap.containsKey("context")) {
      Map<String, Object> contextMap = (Map<String, Object>) jwtMap.get("context");
      if (contextMap.containsKey("user")) {
        userMap = (Map<String, String>) contextMap.get("user");
      }
    }
    return userMap;
  }

}
