package com.ujg.service;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.AtlassianConnectProperties;

/**
 * Copied from AtlassianConnectContextModelAttributeProvider.java
 */
@Component
public class ConnectContextParamsProvider {

  private static final String ALL_JS_FILENAME = "all.js";

  private static final String ALL_DEBUG_JS_FILENAME = "all-debug.js";

  @Autowired
  private HttpServletRequest request;

  @Autowired
  private AtlassianConnectProperties atlassianConnectProperties;

  public String getLicense() {
    return request.getParameter("lic");
  }

  public String getLocale() {
    return request.getParameter("loc");
  }

  public String getTimezone() {
    return request.getParameter("tz");
  }

  public String getAllJsUrl() {
    return getHostBaseUrl().map(this::createAllJsUrl).orElse("");
  }

  public Optional<String> getHostBaseUrl() {
    Optional<String> optionalBaseUrl = getHostBaseUrlFromPrincipal();
    if (!optionalBaseUrl.isPresent()) {
      optionalBaseUrl = getHostBaseUrlFromQueryParameters();
    }
    return optionalBaseUrl;
  }

  private Optional<String> getHostBaseUrlFromPrincipal() {
    return Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication()).map(Authentication::getPrincipal)
        .filter(AtlassianHostUser.class::isInstance).map(AtlassianHostUser.class::cast).map(AtlassianHostUser::getHost)
        .map(AtlassianHost::getBaseUrl);
  }

  private Optional<String> getHostBaseUrlFromQueryParameters() {
    String hostUrl = request.getParameter("xdm_e");
    String contextPath = request.getParameter("cp");

    Optional<String> optionalBaseUrl = Optional.empty();
    if (!StringUtils.isEmpty(hostUrl)) {
      if (!StringUtils.isEmpty(contextPath)) {
        optionalBaseUrl = Optional.of(hostUrl + contextPath);
      } else {
        optionalBaseUrl = Optional.of(hostUrl);
      }
    }
    return optionalBaseUrl;
  }

  private String createAllJsUrl(String hostBaseUrl) {
    return String.format("%s/%s/%s", hostBaseUrl, "atlassian-connect", getAllJsFilename());
  }

  private String getAllJsFilename() {
    return atlassianConnectProperties.isDebugAllJs() ? ALL_DEBUG_JS_FILENAME : ALL_JS_FILENAME;
  }
}
