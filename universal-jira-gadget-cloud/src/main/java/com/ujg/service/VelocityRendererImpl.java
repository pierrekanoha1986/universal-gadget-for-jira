package com.ujg.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.ujg.common.model.User;
import com.ujg.common.service.AuthenticationContext;
import com.ujg.common.service.I18n;
import com.ujg.common.service.VelocityRenderer;
import com.ujg.common.utils.CommonUtils;

@Component
public class VelocityRendererImpl implements VelocityRenderer {

  /**
   * Matches model attribute from AtlassianConnectContextModelAttributeProvider.
   */
  private static final String AC_ALL_JS_URL_PARAM = "atlassian-connect-all-js-url";

  /**
   * Matches model attribute from AtlassianConnectContextModelAttributeProvider.
   */
  private static final String AC_LICENSE_PARAM = "atlassian-connect-license";

  /**
   * Matches model attribute from AtlassianConnectContextModelAttributeProvider.
   */
  private static final String AC_LOCALE_PARAM = "loc";

  /**
   * Matches model attribute from AtlassianConnectContextModelAttributeProvider.
   */
  private static final String AC_TIMEZONE_PARAM = "tz";

  /**
   * AuthenticationContext.
   */
  @Autowired
  private AuthenticationContext authenticationContext;

  /**
   * ConnectContextParamsProvider.
   */
  @Autowired
  private ConnectContextParamsProvider connectParamsProvicer;

  @Autowired
  private VelocityEngine engine;

  @Autowired
  private I18n i18n;

  @Value("${pluginVersion}")
  private String pluginVersion;

  @Autowired
  private HttpServletRequest request;

  @Override
  public void renderTemplate(@NotNull final HttpServletRequest req, @NotNull final HttpServletResponse resp,
      @NotNull final String template, @NotNull final Map<String, Object> contextParams) throws IOException {
    contextParams.put(REQUEST_PARAM, req);
    resp.setContentType("text/html; charset=UTF-8");
    resp.getWriter().write(renderTemplate(template, contextParams));
  }

  @Override
  public String renderTemplate(@NotNull final String templatePath) {
    return doRenderTemplate(templatePath, new HashMap<>());
  }

  @Override
  public String renderTemplate(@NotNull final String templatePath, @NotNull final Map<String, Object> contextParams) {
    Map<String, Object> contextParamsMap = new HashMap<>(contextParams);
    return doRenderTemplate(templatePath, contextParamsMap);
  }

  private String doRenderTemplate(final String templatePath, final Map<String, Object> contextParams) {
    contextParams.put(IS_CLOUD_PARAM, true);
    contextParams.put(RESOURCES_PATH_PARAM, request.getContextPath());
    contextParams.put(I18N_PARAM, i18n);
    contextParams.put(PLUGIN_VERSION, pluginVersion);
    // put the same params as AtlassianConnectContextModelAttributeProvider
    // would put for spring controller.
    contextParams.put(HOST_BASE_URL, connectParamsProvicer.getHostBaseUrl().orElse(""));
    contextParams.put(AC_LICENSE_PARAM, connectParamsProvicer.getLicense());
    contextParams.put(AC_LOCALE_PARAM, connectParamsProvicer.getLocale());
    contextParams.put(AC_TIMEZONE_PARAM, connectParamsProvicer.getTimezone());
    contextParams.put(AC_ALL_JS_URL_PARAM, connectParamsProvicer.getAllJsUrl());
    // pass user to make it available for JS
    User user = authenticationContext.getUser();
    if (user != null) {
      String userJson = CommonUtils.objectToJsonString(user);
      contextParams.put(VelocityRenderer.USER_JSON_OBJ, CommonUtils.escapeHtml(userJson)); 
    }

    return VelocityEngineUtils.mergeTemplateIntoString(this.engine, templatePath, "UTF-8", contextParams);
  }

}