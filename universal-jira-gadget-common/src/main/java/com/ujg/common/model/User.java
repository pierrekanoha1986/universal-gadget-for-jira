package com.ujg.common.model;

public class User {

  private String displayName;

  private String userKey;

  private String username;

  public User(String userKey, String username, String displayName) {
    this.userKey = userKey;
    this.username = username;
    this.displayName = displayName;
  }

  /**
   * @return the displayName
   */
  public String getDisplayName() {
    return displayName;
  }

  /**
   * @return the userKey
   */
  public String getUserKey() {
    return userKey;
  }

  /**
   * @return the username
   */
  public String getUsername() {
    return username;
  }

}