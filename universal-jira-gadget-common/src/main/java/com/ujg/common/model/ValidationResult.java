package com.ujg.common.model;

import java.util.ArrayList;
import java.util.Collection;

public class ValidationResult {

  /**
   * Errors collection.
   */
  private Collection<String> errors;

  /**
   * Is valid flag.
   */
  private boolean isValid;

  /**
   * @param isValid
   */
  public ValidationResult(boolean isValid) {
    this.errors = new ArrayList<>();
    this.isValid = isValid;
  }

  /**
   * @param errorCollection
   * @param isValid
   */
  public ValidationResult(Collection<String> errors, boolean isValid) {
    this.errors = errors;
    this.isValid = isValid;
  }

  /**
   * @param errors the errors to set
   */
  public void addError(String error) {
    this.errors.add(error);
  }

  /**
   * @return the errors
   */
  public Collection<String> getErrors() {
    return errors;
  }

  /**
   * @return the isValid
   */
  public boolean isValid() {
    return isValid;
  }

  /**
   * @param errors the errors to set
   */
  public void setErrors(Collection<String> errors) {
    this.errors = errors;
  }

  /**
   * @param isValid the isValid to set
   */
  public void setValid(boolean isValid) {
    this.isValid = isValid;
  }

}