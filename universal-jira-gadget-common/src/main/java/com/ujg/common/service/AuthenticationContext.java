package com.ujg.common.service;

import com.ujg.common.model.User;

public interface AuthenticationContext {

  /**
   * @return User or null.
   */
  public User getUser();

}