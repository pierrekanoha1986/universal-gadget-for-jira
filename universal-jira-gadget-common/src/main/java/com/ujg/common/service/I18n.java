package com.ujg.common.service;

public interface I18n {

  /**
   * 
   * @param propertyKey
   * @return
   */
  public String getText(final String propertyKey);

  /**
   * 
   * @param propertyKey
   * @param params
   * @return
   */
  public String getText(final String propertyKey, Object... params);

}
