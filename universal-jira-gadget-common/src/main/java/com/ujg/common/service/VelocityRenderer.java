package com.ujg.common.service;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

public interface VelocityRenderer {

  /**
   * Host base URL.
   */
  static String HOST_BASE_URL = "hostBaseUrl";

  static String I18N_PARAM = "i18n";

  static String IS_CLOUD_PARAM = "isCloud";

  static String PLUGIN_VERSION = "pluginVersion";

  static String REQUEST_PARAM = "req";

  static String RESOURCES_PATH_PARAM = "resourcesPath";

  /**
   * Token param.
   */
  static String TOKEN_PARAM = "acpt";

  static String USER_JSON_OBJ = "userJson";

  /**
   * 
   * @param req
   * @param resp
   * @param template
   * @param contextParams
   * @throws IOException
   */
  void renderTemplate(@NotNull final HttpServletRequest req, @NotNull final HttpServletResponse resp,
      @NotNull final String template, @NotNull final Map<String, Object> contextParams) throws IOException;

  /**
   * Renders template into string.
   * 
   * @param templatePath
   * @return
   */
  String renderTemplate(@NotNull final String templatePath);

  /**
   * Renders template into string.
   * 
   * @param templatePath
   * @param contextParams
   * @return
   */
  String renderTemplate(@NotNull final String templatePath, @NotNull final Map<String, Object> contextParams);

}