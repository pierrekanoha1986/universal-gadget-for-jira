package com.ujg.common.utils;

import org.apache.commons.lang.StringEscapeUtils;

import com.google.gson.Gson;

public class CommonUtils {

  /**
   * Shared Json object.
   */
  private static final Gson gson = new Gson();

  /**
   * Escapes the characters in a String using HTML entities.
   * For example: * "bread" & "butter" becomes: &quot;bread&quot; &amp; &quot;butter&quot;.
   * 
   * @param inputStr
   * @return
   */
  public static String escapeHtml(String inputStr) {
    return StringEscapeUtils.escapeHtml(inputStr);
  }

  /**
   * Converts to object.
   * 
   * @param obj
   * @param jsonString
   * @return
   */
  public static <T> T jsonStringToObject(String jsonString, Class<T> clazz) {
    return gson.fromJson(jsonString, clazz);
  }

  /**
   * Converts to JSON string.
   * 
   * @param obj
   * @return
   */
  public static String objectToJsonString(Object obj) {
    return gson.toJson(obj);
  }

}
