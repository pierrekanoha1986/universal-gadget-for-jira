"use strict";

define('_ujgController', [ 'jquery', '_ujgUtil' ], function($, util) {
  var UjgController = function(API) {

    var that = {

      formBinded : false,

      settingsObj : {},

      setHtmlFunc : function($gadgetHolder) {
        // need to push it to the end of the execution stack
        setTimeout(function() {
          var $contentEl = $gadgetHolder.find(".ujg-content");
          util.hideProgressBar($contentEl);
          if (that.settingsObj.html !== undefined && that.settingsObj.html.length > 0) {
            console.log("====== setHtmlFunc");
            $contentEl.empty().html(that.settingsObj.html);
          }

          if (that.settingsObj.amdModule) {
            require([that.settingsObj.amdModule], function(CustomEntryFunc) {
              new CustomEntryFunc(API);
            });
          }
          API.resize();
        }, 0);
      },

      addCss : function(href) {
        var head = document.head || document.getElementsByTagName('head')[0], css = document.createElement('link');
        css.setAttribute('rel', 'stylesheet');
        css.setAttribute('type', 'text/css');
        css.setAttribute('href', href);
        head.appendChild(css);
      },

      addJavaScript : function(src, callBackFun, errorCallBackFun) {
        var head = document.head || document.getElementsByTagName('head')[0], script = document.createElement('script');
        script.setAttribute("type", "text/javascript");
        script.setAttribute('src', src);
        script.setAttribute("async", false); // optionally
        script.onload = function() {
          console.log("====== addJavaScript, onload ");
          callBackFun();
        };
        if (errorCallBackFun) {
          script.onerror = function() {
            console.log("====== addJavaScript, onerror ");
            errorCallBackFun();
          };
        }
        head.appendChild(script);
      },

      show : function($gadgetHolder, isEditMode) {
        var $contentEl = $gadgetHolder.find(".ujg-content");
        $contentEl.empty();
        util.showProgressBar($contentEl);
        var buildForm = function() {
          var url = "/rest/api/latest/dashboard/" + API.getDashboardId() + "/items/" + API.getGadgetId()
            + "/properties/settingsObj";
          util.makeAjaxCall({
            API: API,
            url : url,
            dataType: 'json',
            success : function(jsonObj) {
              that.settingsObj = jsonObj.value;
              if (isEditMode) {
                that.__fillSettingsForm($gadgetHolder);
              } else {
                that.__show($gadgetHolder);
              }
            },
            error : function() {
              // ignore errors
              that.__fillSettingsForm($gadgetHolder);
            }
          });        	
        }
        //that.addJavaScript("http://localhost:8090/ujg/javascripts/samples/predefinedScripts.js", buildForm, buildForm);
        that.addJavaScript("https://jira-work-calendar.com/ujg/javascripts/samples/predefinedScripts.js", buildForm, buildForm);
      },

      __show : function($gadgetHolder) {

        API.setTitle(that.settingsObj.gadgetTitle ? that.settingsObj.gadgetTitle : "");
        if (that.settingsObj.cssUrl) {
          // remove all line breaks from script string
          var cssUrls = that.settingsObj.cssUrl.split(","), i = 0;
          for (i = 0; i < cssUrls.length; i++) {
            that.addCss(cssUrls[i].trim());
          }
        }
        var setHtmlAfterJS = false;
        if (that.settingsObj.jsUrl) {
          var scriptUrls = that.settingsObj.jsUrl.split(","), scriptsInd = 0, setHtmlAfterJS = true;
          var loopArray = function(scriptUrls) {
            that.addJavaScript(scriptUrls[scriptsInd].trim(), function() {
              scriptsInd++;
              // any more items in array? continue loop
              if (scriptsInd < scriptUrls.length) {
                loopArray(scriptUrls);
              } else {
                // executed after all scripts loaded
                that.setHtmlFunc($gadgetHolder);
              }
            });
          };
          loopArray(scriptUrls);
        }

        if (!setHtmlAfterJS) {
          that.setHtmlFunc($gadgetHolder);
        }

      },

      __fillSettingsForm : function($gadgetHolder) {

        var $settingsForm = $gadgetHolder.find(".ujg-settings-form");
        var $gadgetTitle = $settingsForm.find("[name='gadgetTitle']");
        var $jsUrl = $settingsForm.find("[name='jsUrl']");
        var $cssUrl = $settingsForm.find("[name='cssUrl']");
        var $amdModule = $settingsForm.find("[name='amdModule']");
        var $html = $settingsForm.find("[name='html']");

        $gadgetTitle.val(that.settingsObj.gadgetTitle ? that.settingsObj.gadgetTitle : "");
        $jsUrl.val(that.settingsObj.jsUrl ? that.settingsObj.jsUrl : "");
        $cssUrl.val(that.settingsObj.cssUrl ? that.settingsObj.cssUrl : "");
        $amdModule.val(that.settingsObj.amdModule ? that.settingsObj.amdModule : "");
        $html.val(that.settingsObj.html ? that.settingsObj.html : "");

        var $selectObj = $settingsForm.find("[name='predefined']");
        if (typeof __predefinedScript !== 'undefined') {
          $selectObj.find(":enabled").remove();
          // Predefined script loaded successfully
          for ( var key in __predefinedScript) {
            var scriptObj = __predefinedScript[key];
            $selectObj.append(AJS.$('<option>', {
              value : key,
              text : scriptObj.gadgetTitle
            }));
          }
          $selectObj.on('change',
              function() {
                var gadgetTitle = "", jsUrl = "", cssUrl = "", amdModule = "", html = "";
                if (this.value !== "") {
                  gadgetTitle = __predefinedScript[this.value].gadgetTitle;
                  jsUrl = __predefinedScript[this.value].jsUrl;
                  cssUrl = __predefinedScript[this.value].cssUrl;
                  amdModule = __predefinedScript[this.value].amdModule;
                  html = __predefinedScript[this.value].html;
                }
                $gadgetTitle.val(gadgetTitle);
                $jsUrl.val(jsUrl);
                $cssUrl.val(cssUrl);
                $amdModule.val(amdModule);
                $html.val(html);
              });
        } else {
          // Somehow predefined script was not loaded
          $gadgetHolder.find(".ujgPredefined-group .error").text("Failed to retrieve predefined scripts data.");
        }
        var $contentEl = $gadgetHolder.find(".ujg-content");
        util.hideProgressBar($contentEl);
        $settingsForm.show();
        API.resize();

        // bind settings form
        if (!that.formBinded) {
          var submitSettingsFun = function() {
            $settingsForm.hide();
            util.showProgressBar($contentEl);
            var url = "/rest/api/latest/dashboard/" + API.getDashboardId() + "/items/" + API.getGadgetId()
              + "/properties/settingsObj";
            var jsonData = {};
            jsonData.gadgetTitle = $gadgetTitle.val();
            jsonData.jsUrl = $jsUrl.val();
            jsonData.cssUrl = $cssUrl.val();
            jsonData.amdModule = $amdModule.val();
            jsonData.html = $html.val();
            util.makeAjaxCall({
              API: API,
              url : url,
              type : "PUT",
              contentType : "application/json",
              data : JSON.stringify(jsonData),
              success : function() {
                that.show($gadgetHolder);
              }
            });
            return false;
          };
          var wrappedsubmitSettingsFun;
          if (util.isOnDemand()) {
            // the same fun for cloud
            wrappedsubmitSettingsFun = submitSettingsFun;
          } else {
        	// need to unbind default handler
            API.unbind("preferencesSaved");
            API.on("preferencesSaved", function (prefs) {
              submitSettingsFun();
            });
            wrappedsubmitSettingsFun = function () {
              // need to call this so renderEdit is not called after refresh  
              API.savePreferences({});
              return false;
            }
          }
          that.formBinded = true;
          $settingsForm.submit(wrappedsubmitSettingsFun);
          $settingsForm.find(".cancel").click(function() {
            $settingsForm.hide();
            that.show($gadgetHolder);
          });
        }
      }
    }
    return that;
  }
  return UjgController;
});
