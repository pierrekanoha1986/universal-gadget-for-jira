"use strict";

require([ 'jquery', '_ujgController' ], function($, Controller) {

  // API object
  var API = {

    getCurrentUser : function() {
      return this.currentUser;
    },

    getDashboardId : function(title) {
      return this.dashboardId;
    },

    getGadgetContentEl : function(title) {
      return this.$gadgetContentEl;
    },

    getGadgetId : function(title) {
      return this.gadgetId;
    },

    resize : function() {
      AP.resize(undefined, this.$gadgetHolder.outerHeight(true));
    },

    setTitle : function(title) {
      AP.require([ 'jira' ], function(jira) {
        jira.setDashboardItemTitle(title);
      });
    }

  };

  $(document).ready(function() {
    API.currentUser = $.parseJSON($('meta[name=_ujg-user]').attr("content"));
    API.dashboardId = $('meta[name=_ujg-dashboardId]').attr("content");
    API.$gadgetHolder = $(".ujg-panel");
    API.$gadgetContentEl = API.$gadgetHolder.find(".ujg-content");
    API.gadgetId = $('meta[name=_ujg-dashboardItemId]').attr("content");

    var controller = new Controller(API);
    controller.show(API.$gadgetHolder);
    AP.require([ 'jira' ], function(jira) {
      jira.DashboardItem.onDashboardItemEdit(function() {
        controller.show(API.$gadgetHolder, true);
      });
    });
  });

});