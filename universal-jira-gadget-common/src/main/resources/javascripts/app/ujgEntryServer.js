"use strict";

define("_ujgEntryServer", [ 'jquery', "_ujgController" ], function($, Controller) {

  var DashboardItem = function(API) {

    var $gadgetContentEl = API.publicInstace.getElement().find(".ujg-panel .ujg-content");
    
    var currentUser = $.parseJSON($('meta[name=_ujg-user]').attr("content"));

	// additional methods so we have common API for Cloud and Server versions

    API.getDashboardId = function() {
      return API.getContext().dashboard.id;
    };

    API.getCurrentUser = function() {
      return currentUser;
    };

    API.getGadgetContentEl = function(title) {
      return $gadgetContentEl;
    };

    this.controller = new Controller(API);
  };

  /**
   * Called to render the view for a fully configured dashboard item.
   * 
   * @param context The surrounding <div/> context that this items should render into.
   * @param preferences The user preferences saved for this dashboard item (e.g. filter id, number of results...)
   */
  DashboardItem.prototype.render = function(context, preferences) {
    this.controller.show(context.find(".ujg-panel"));
  };

  /**
   * Called to render the configuration form for this dashboard item if preferences.isConfigured has not been set yet.
   * 
   * @param context The surrounding <div/> context that this items should render into.
   * @param preferences The user preferences saved for this dashboard item
   */
  DashboardItem.prototype.renderEdit = function(context, preferences) {
    this.controller.show(context.find(".ujg-panel"), true);
  };

  return DashboardItem;

});