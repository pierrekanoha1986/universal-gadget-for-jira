define('_ujgUtil', [ 'jquery' ], function($) {
  var that = {

    onDemand : $('meta[name=_ujg-isCloud]').attr("content") === 'true',

    jiraBaseUrl : $('meta[name=_ujg-hostBaseUrl]').attr("content"),

    jiraServerVersion : $('meta[name=ajs-version-number]').attr("content"),

    pluginVersion : $('meta[name=_ujg-version]').attr("content"),

    isOnDemand : function() {
      return that.onDemand;
    },

    getJiraBaseUrl : function() {
      return that.jiraBaseUrl;
    },

    getJiraServerVersion : function() {
      return that.jiraServerVersion;
    },

    getPluginVersion : function() {
      return that.pluginVersion;
    },

    makeAjaxCall : function(ajaxParams) {

      if (!ajaxParams.error) {
        if (!ajaxParams.API) {
          // check to push context passing
          console.error("Plese pass API that could be needed for displaying error messages.");
          return;
        }
        ajaxParams.error = function(jqXHR, textStatus, errorThrown) {
          that.handleError(API, ajaxParams.url, jqXHR, textStatus, errorThrown);
          that.hideProgressBar(ajaxParams.context);
        };
      }

      if (that.isOnDemand()) {
    	if (ajaxParams.dataType && ajaxParams.dataType === "json") {
          var successFun = ajaxParams.success;
    	  ajaxParams.success =  function(responseText) {
            // AP.request brings it as a text
            successFun($.parseJSON(responseText));
          };
    	}
        AP.request(ajaxParams);
      } else {
        ajaxParams.url = that.getJiraBaseUrl() + ajaxParams.url;
        $.ajax(ajaxParams);
      }
    },

    handleError : function(API, url, jqXHR, textStatus, errorThrown) {

      if (errorThrown == null) {
        if (jqXHR != null && jqXHR.statusText != null) {
          errorThrown = jqXHR.statusText;
        } else {
          errorThrown = "";
        }
      }
      var responseText = "";
      if (jqXHR != null) {
        responseText = jqXHR.responseText;
        if (responseText.length > 600) {
          responseText = responseText.substring(0, 600) + "...";
        }
      }
      var status = jqXHR != null ? jqXHR.status : "";
      var sep = "\n======================================================\n";
      var reportError = 'If you think its a bug, please report to https://vkrupach.atlassian.net' + sep
          + 'Is Cloud: ' + that.isOnDemand()
          + (that.isOnDemand() ? "" : ', JIRA Server version: ' + that.getJiraServerVersion()) + ', addon version: '
          + that.getPluginVersion() + ', error thrown: ' + errorThrown + ', host: ' + that.getJiraBaseUrl()
          + ', response text: ' + responseText + ', status: ' + status + ', status text: ' + textStatus + ', url: ' + url
          + sep;
      that.showMessage(API.getGadgetContentEl(), "error", "Error" + (textStatus != null ? ": " + textStatus : ""), reportError);
      API.resize();
    },

    // type: generic, error, warning, success, info, hint, see: https://docs.atlassian.com/aui/latest/docs/messages.html
    showMessage : function(context, type, title, text, fadeout) {
      AJS.messages[type](context, {
        title : title,
        body : AJS.escapeHtml(text),
        fadeout : fadeout
      });
    },

    showProgressBar : function($parentEl) {
      // copied from connect-messages.js
      if ($parentEl.find(".aui-progress-indicator").length === 0) { 
        $parentEl.append('<div class="progress-indicator-cust"><div class="aui-progress-indicator">'
          + '<span class="aui-progress-indicator-value progress-indicator-value-cust"></span></div></div>');
      }
    },

    hideProgressBar : function($parentEl) {
      // copied from connect-messages.js
      $parentEl.find(".progress-indicator-cust").remove();
    }

  }

  return that;
});