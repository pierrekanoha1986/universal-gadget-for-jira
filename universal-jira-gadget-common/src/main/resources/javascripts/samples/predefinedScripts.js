// local url http://localhost:8090/ujg/
var __predefinedScript = {}, __predefinedScriptsUrl = "https://jira-work-calendar.com/ujg/javascripts/samples"; 
__predefinedScript.apiCalls = {};
__predefinedScript.apiCalls.gadgetTitle = "API Calls";
__predefinedScript.apiCalls.jsUrl = "";
__predefinedScript.apiCalls.cssUrl = "";
__predefinedScript.apiCalls.amdModule = "_ujgApiCalls";
__predefinedScript.apiCalls.html = '<script>\n'
+ 'define("_ujgApiCalls", [ "jquery" ], function($) {\n'
+ '  var MyGadget = function(API) {\n'
+ '    var $contentEl = API.getGadgetContentEl();\n'
+ '    $contentEl.append("<p>Called API.getGadgetContentEl() to retrieve the gadget content element.</p>");\n'
+ '    API.setTitle("API calls");\n'
+ '    $contentEl.append("<p>Called API.setTitle() to set \'API calls\'.</p>");\n'
+ '    var user = API.getCurrentUser();\n'
+ '    $contentEl.append("<p>API.getCurrentUser(), user.userKey: " + user.userKey + ", user.username: " + user.username + ", user.displayName: " + user.displayName + ".");\n'
+ '    var dashboardId = API.getDashboardId(), gadgetId = API.getGadgetId();\n'
+ '    $contentEl.append("<p>API.getDashboardId(): " + dashboardId + ", API.getGadgetId(): " + gadgetId + ". These can be needed for REST API calls.</p>");\n'
+ '    $contentEl.append("<p>You may need to call API.resize() to fit the gadget height when you added/edited it\'s content in callback functions.</p>");\n'
+ '    API.resize();\n'
+ '  };\n'
+ '  return MyGadget;\n'
+ '});\n'
+ '</script>';

__predefinedScript.civTest = {};
__predefinedScript.civTest.gadgetTitle = "Civilization Test";
__predefinedScript.civTest.jsUrl = "";
__predefinedScript.civTest.cssUrl = "";
__predefinedScript.civTest.amdModule = "_ujgCivilizationTest";
__predefinedScript.civTest.html = '<script>\n'
+ 'define("_ujgCivilizationTest", [ "jquery" ], function($) {\n'
+ '  var MyGadget = function(API) {\n'
+ '    var $msgDiv = $(\'<div style="padding:10px;"></div>\');\n'
+ '    API.getGadgetContentEl().append($msgDiv);\n'
+ '    var showMessageFun = function(civStatus) {\n'
+ '      AJS.messages[civStatus ? \'success\' : \'error\']($msgDiv, {\n'
+ '        title: civStatus ? \'No worries.\' : \'Alarm!\',\n'
+ '        body: civStatus ? \'The civilization still exists.\' : \'Is it the end of the civilization?\'\n'
+ '      });\n'
+ '      API.resize();\n'
+ '    };\n'
+ '    $.ajax({\n'
+ '      url : \'//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js\',\n'
+ '      complete: function(e, xhr) {\n'
+ '        showMessageFun(e.status !== 0 && e.status <= 400);\n'
+ '      }\n'
+ '    });\n'
+ '  };\n'
+ '  return MyGadget;\n'
+ '});\n'
+ '</script>';

__predefinedScript.weahter = {};
__predefinedScript.weahter.gadgetTitle = "Weather";
__predefinedScript.weahter.jsUrl = __predefinedScriptsUrl + "/weather/weather.js";
__predefinedScript.weahter.cssUrl = __predefinedScriptsUrl + "/weather/weather.css";
__predefinedScript.weahter.amdModule = "";
__predefinedScript.weahter.html = '<div id="weatherInLviv" class="ujg-weather"></div>\n'
    + '<script>\njQuery(document).ready(function() {\n'
    + 'jQuery.simpleWeather({\n'
    + 'location : "Lviv, Ukraine",\n'
    + 'unit : "c",\n'
    + 'success : function(weather) {\n'
    + 'html = "<h2><i class=\\"icon-"+weather.code+"\\"></i> " + weather.temp + "&deg;" + weather.units.temp'
    + '+ "</h2><ul><li>" + weather.city + (weather.region !== "" ? ", " + weather.region : "") + "</li>'
    + '<li class=\\"currently\\">" + weather.currently + "</li>"'
    + '+ "<li>" + weather.wind.direction + " " + weather.wind.speed + " " + weather.units.speed'
    + '+ "</li></ul>";\n'
    + 'for(var i=1;i<weather.forecast.length;i++) {\n'
    + 'html += "<div class=\'forecast-day\'>" + weather.forecast[i].day + " " + weather.forecast[i].date.split(" ")[0] + \n'
    + '"<br/><span class=\'forecast-icon icon-" + weather.forecast[i].code + "\'></span><br/>" + weather.forecast[i].high + "&deg;/"\n'
    + '+ weather.forecast[i].low + "&deg;</div>"; \n' + '}\n' + 'jQuery("#weatherInLviv").html(html);' + '},\n'
    + 'error : function(error) {\n' + 'jQuery("#weatherInLviv").html("<p>" + error + "</p>");}\n});\n});\n</script>';

__predefinedScript.userTimesheet = {};
__predefinedScript.userTimesheet.gadgetTitle = "User timesheet";
__predefinedScript.userTimesheet.jsUrl = __predefinedScriptsUrl + "/timesheet/util.js, " + __predefinedScriptsUrl + "/timesheet/eventsProvider.js, " + 
  __predefinedScriptsUrl + "/timesheet/tableDrawer.js";
__predefinedScript.userTimesheet.cssUrl = __predefinedScriptsUrl + "/timesheet/userTimesheet.css";
__predefinedScript.userTimesheet.amdModule = "_ujgTimesheet";
__predefinedScript.userTimesheet.html = '<div class="ujg-timesheet"></div>\n'
    + '<script>\n'
    + 'define("_ujgTimesheet", [ "jquery", "_ujgTimeEventsProvider", "_ujgTimeTableDrawer" ], function($, EventsProvider, tableDrawer) {\n'
    + '  var MyGadget = function(API) {\n'
    + '    var dataParams = {};\n'
    + '    // end date is exclusive, so add 1 day\n'
    + '    dataParams.end = new Date((new Date()).getTime() + (24 * 60 * 60 * 1000))\n'
    + '    dataParams.end.setHours(0, 0, 0, 0);\n'
    + '    // start date is inclusive, set it 1 week back\n'
    + '    dataParams.start = new Date(dataParams.end.getTime() - (15 * 24 * 60 * 60 * 1000));\n'
    + '    // dataParams.jql = "status = "In Progress"";\n'
    + '    // dataParams.allUsers = true;\n'
    + '    // array of usernames\n'
    + '    dataParams.usernames = [API.getCurrentUser().username];\n'
    + '    var eventsProvider = new EventsProvider(API);\n'
    + '    eventsProvider.getEvents(dataParams, function(eventsArr) {\n'
    + '      var $contDiv = API.getGadgetContentEl().find(".ujg-timesheet");\n'
    + '      tableDrawer.drawByIssue(dataParams, eventsArr, $contDiv);\n'
    + '      API.resize();\n'
    + '    });\n'
    + '  //.text("You have to be logged in.")\n'
    + '  };\n'
    + '  return MyGadget;\n'
    + '});'
    + '</script>';