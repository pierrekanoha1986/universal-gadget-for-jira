package com.ujg;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

/**
 * Checks if the current user is the owner of a given dashboard
 *
 * @since v4.0
 */
public class IsDashboardCondition implements Condition {

  public static final String CONTEXT_KEY_DASHBOARD_ID = "dashboardId";

  public void init(final Map<String, String> params) throws PluginParseException {
  }

  public boolean shouldDisplay(final Map<String, Object> context) {
    final Object o = context.get("request");

    if (o != null && o instanceof HttpServletRequest) {
      HttpServletRequest request = (HttpServletRequest) o;
      if (request.getServletPath().endsWith("Dashboard.jspa")
          || request.getServletPath().endsWith("EditDefaultDashboard!default.jspa")) {
        return true;
      }
    }
    return false;
  }

}