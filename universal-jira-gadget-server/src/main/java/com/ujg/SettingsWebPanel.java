package com.ujg;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.plugin.web.model.WebPanel;
import com.ujg.common.model.User;
import com.ujg.common.service.AuthenticationContext;
import com.ujg.common.utils.CommonUtils;

/**
 * We pass Java settings to JavaScript through meta tags.
 */
public class SettingsWebPanel implements WebPanel {

  @Autowired
  private AuthenticationContext authenticationContext;

  /**
   * Form meta tags that can be read from JavaScript
   */
  public String getHtml(Map<String, Object> stringObjectMap) {
    StringBuilder metaHtml = new StringBuilder("\n");
    metaHtml.append(createMetaLine("_ujg-version", Utils.getPluginVersion()));
    User user = authenticationContext.getUser();
    if (user != null) {
      String userJson = CommonUtils.objectToJsonString(user);
      metaHtml.append(createMetaLine("_ujg-user", userJson));
    }
    metaHtml.append(createMetaLine("_ujg-isCloud", "false"));
    metaHtml.append(createMetaLine("_ujg-hostBaseUrl", ComponentAccessor.getApplicationProperties().getString(APKeys.JIRA_BASEURL)));
    return metaHtml.toString();
  }

  @Override
  public void writeHtml(Writer writer, Map<String, Object> context) throws IOException {
    writer.write(getHtml(context));
  }

  /**
   * Creates meta tag line
   * 
   * @param tagName
   * @param value
   * @return Meta tag line
   */
  private String createMetaLine(String tagName, String value) {
    return "<meta name=\"" + tagName + "\" content=\"" + CommonUtils.escapeHtml(value) + "\" />\n";
  }

}