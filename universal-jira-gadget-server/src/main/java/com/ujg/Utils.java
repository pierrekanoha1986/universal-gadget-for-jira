package com.ujg;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginInformation;

public class Utils {

  /**
   * groupId + "." + artifactId
   */
  public static final String PLUGIN_KEY = "com.jiraworkcalendar.ujg";

  /**
   * Version of the plugin
   */
  public static String version;

  public static String getPluginVersion() {
    if (version == null) {
      PluginAccessor pa = ComponentAccessor.getPluginAccessor();
      PluginInformation pi = pa.getPlugin(PLUGIN_KEY).getPluginInformation();
      version = pi.getVersion();
    }
    return version;
  }

}
