package com.ujg.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.ujg.common.model.User;
import com.ujg.common.service.AuthenticationContext;

@Component
public class AuthenticationContextImpl implements AuthenticationContext {

  /**
   * JiraAuthenticationContext
   */
  @Autowired
  private JiraAuthenticationContext jiraAuthenticationContext;

  @Override
  public User getUser() {
    User user = null;
    ApplicationUser appUser = jiraAuthenticationContext.getLoggedInUser();
    if (appUser != null) {
      user = new User(appUser.getKey(), appUser.getUsername(), appUser.getDisplayName());
    }
    return user;
  }

}
